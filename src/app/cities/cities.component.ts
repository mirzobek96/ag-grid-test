import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '../Services/api.service';
import {IGetRowsParams} from 'ag-grid-community';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent {
  private gridApi;
  private gridColumnApi;

  private columnDefs;
  private defaultColDef;
  private components;
  private rowBuffer;
  private rowSelection;
  private rowModelType;
  private paginationPageSize;
  private cacheOverflowSize;
  private maxConcurrentDatasourceRequests;
  private infiniteInitialRowCount;
  private maxBlocksInCache;
  private rowData: [];

  constructor(private http: HttpClient, private apiService: ApiService) {
    this.columnDefs = [
      {
        field: 'district',
        minWidth: 150,
      },
      { field: 'subject' },
      {
        field: 'name',
      },
      { field: 'population' }
    ];
    this.defaultColDef = {
      flex: 1,
      resizable: true,
      minWidth: 100,
    };
    this.components = {
      loadingRenderer: function (params) {
        if (params.value !== undefined) {
          return params.value;
        } else {
          return "<img src=\"https://www.ag-grid.com/example-assets/loading.gif\">";
        }
      },
    };
    this.rowBuffer = 0;
    this.rowSelection = 'multiple';
    this.rowModelType = 'infinite';
    this.paginationPageSize = 100;
    this.cacheOverflowSize = 2;
    this.maxConcurrentDatasourceRequests = 1;
    this.infiniteInitialRowCount = 1000;
    this.maxBlocksInCache = 10;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.http
      .get('assets/cities.json')
      .subscribe((data: []) => {
        const dataSource = {
          rowCount: null,
          getRows: (param) => {
            console.log(
              'asking for ' + param.startRow + ' to ' + param.endRow
            );
            setTimeout(() => {
              const rowsThisPage = data.slice(param.startRow, param.endRow);
              let lastRow = -1;
              if (data.length <= param.endRow) {
                lastRow = data.length;
              }
              param.successCallback(rowsThisPage, lastRow);
            }, 500);
          },
        };
        params.api.setDatasource(dataSource);
      });
  }
}
